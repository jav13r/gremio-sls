package lib

import (
	//Go packages
	"bytes"
	"encoding/json"
	"fmt"

	//Third party packages
	"github.com/aws/aws-lambda-go/events"
	"github.com/xeipuuv/gojsonschema"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
type Response events.APIGatewayProxyResponse

// EmptyResponse una respuesta sin body
func EmptyResponse(statusCode int) events.APIGatewayProxyResponse {
	resp := events.APIGatewayProxyResponse{
		StatusCode:      statusCode,
		IsBase64Encoded: false,
		Headers: map[string]string{
			"Content-Type": "text/plain",
		},
	}
	return resp
}

// JSONResponse receives a JSON body and a code and returns a Response of type APIGatewayProxyResponse
func JSONResponse(statusCode int, JSONBody []byte) events.APIGatewayProxyResponse {
	var buf bytes.Buffer
	json.HTMLEscape(&buf, JSONBody)
	resp := events.APIGatewayProxyResponse{
		StatusCode:      statusCode,
		IsBase64Encoded: false,
		Body:            buf.String(),
		Headers: map[string]string{
			"Content-Type":                     "application/json",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
		},
	}
	return resp
}

func JSONError(statusCode int, err error) events.APIGatewayProxyResponse {

	body, _ := json.Marshal(map[string]interface{}{
		"message": err.Error(),
	})
	return JSONResponse(statusCode, body)
}

func JSONSchemaError(statusCode int, err []gojsonschema.ResultError) events.APIGatewayProxyResponse {
	responseError := []string{}
	for _, bodyErr := range err {
		data := fmt.Sprintf("%v", bodyErr)
		responseError = append(responseError, data)
	}

	body, _ := json.Marshal(map[string]interface{}{
		"message": responseError,
	})
	return JSONResponse(statusCode, body)
}

func JSONStringResponse(statusCode int, text string) events.APIGatewayProxyResponse {
	resp := events.APIGatewayProxyResponse{
		StatusCode:      statusCode,
		IsBase64Encoded: false,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body: text,
	}
	return resp
}

func JSONMultipleErrors(statusCode int, err []interface{}) events.APIGatewayProxyResponse {

	body, _ := json.Marshal(map[string]interface{}{
		"message": err,
	})

	return JSONResponse(statusCode, body)
}

func JSONSchemaErrorWithExtra(statusCode int, err error, extra interface{}) events.APIGatewayProxyResponse {
	body, _ := json.Marshal(map[string]interface{}{
		"message": err.Error(),
		"extra":   extra,
	})
	return JSONResponse(statusCode, body)
}
