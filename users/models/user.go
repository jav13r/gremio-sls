package models

import "fmt"

type User struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Age      int    `json:"age"`
	Color    string `json:"color"`
	TypeUser int    `json:"type_user"`
}

// methods
func (m User) AgeColor() string {
	return fmt.Sprintf("%v-%v", m.Age, m.Color)
}
