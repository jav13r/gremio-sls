package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/MerqueoTech/gremio-sls/users/models"
	"github.com/MerqueoTech/gremio-sls/users/repositories"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type Handler func(ctx context.Context, req Request) (Response, error)

type UserRepositoryInterface interface {
	Find(ID string) (models.User, error)
}

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
type Response events.APIGatewayProxyResponse
type Request events.APIGatewayProxyRequest

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Adapter(r UserRepositoryInterface) Handler {
	return func(ctx context.Context, req Request) (Response, error) {

		id := req.PathParameters["id"]

		user, err := r.Find(id)
		if err != nil {
			return Response{StatusCode: 280}, err
		}

		body, err := json.Marshal(user)
		if err != nil {
			return Response{StatusCode: http.StatusBadRequest}, err
		}

		resp := Response{
			StatusCode:      200,
			IsBase64Encoded: false,
			Body:            string(body),
			Headers: map[string]string{
				"Content-Type":           "application/json",
				"X-MyCompany-Func-Reply": "hello-handler",
			},
		}

		return resp, nil
	}
}

func main() {

	// variables
	users := os.Getenv("DYNAMO_DEMO")
	if users == "" {
		panic(fmt.Sprintf("DYNAMO_DEMO is empty"))
	}

	// session aws
	session, err := session.NewSession()
	if err != nil {
		panic(fmt.Sprintf("ErrorHasOcurredCreatingAWSSession : %v", err))
	}

	// instanciar dynamo
	dynamo := dynamodb.New(session)

	// instancia repo
	userRepo := repositories.NewUsersRepository(dynamo, users)

	lambda.Start(Adapter(userRepo))
}
