package repositories

import (
	"errors"
	"strconv"

	"github.com/MerqueoTech/gremio-sls/users/models"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/google/uuid"
)

// structure
type UsersRepository struct {
	client *dynamodb.DynamoDB // mysql postgres
	table  string
}

var ErrUsersNotFound = errors.New("error_user_not_found")

// structure methods

func (r *UsersRepository) Hydrate(rows []map[string]*dynamodb.AttributeValue) []models.User {
	user := make([]models.User, len(rows))

	for idx, row := range rows {
		user[idx].ID = *row["id"].S

		if a, ok := row["name"]; ok {
			user[idx].Name = *a.S
		}

		if a, ok := row["age"]; ok {
			val, _ := strconv.Atoi(*a.N)
			user[idx].Age = val
		}

		if a, ok := row["color"]; ok {
			user[idx].Color = *a.S
		}

		if a, ok := row["type_user"]; ok {
			val, _ := strconv.Atoi(*a.N)
			user[idx].TypeUser = val
		}
	}

	return user
}

func (r *UsersRepository) Find(ID string) (models.User, error) {

	out, err := r.client.Query(&dynamodb.QueryInput{
		TableName:              aws.String(r.table),
		KeyConditionExpression: aws.String("id = :ID"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":ID": &dynamodb.AttributeValue{
				S: aws.String(ID),
			},
		},
	})
	if err != nil {
		return models.User{}, err
	}

	if len(out.Items) == 0 {
		return models.User{}, errors.New("not found users")
	}

	return r.Hydrate(out.Items)[0], nil
}

func (r *UsersRepository) FindByAgeColor(age int, color string) ([]models.User, error) {
	sparse := models.User{
		Age:   age,
		Color: color,
	}

	out, err := r.client.Query(&dynamodb.QueryInput{
		TableName:              aws.String(r.table),
		IndexName:              aws.String("by_age_color_sparse"),
		KeyConditionExpression: aws.String("age_color_sparse = :age_color_sparse"),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":age_color_sparse": &dynamodb.AttributeValue{
				S: aws.String(sparse.AgeColor()),
			},
		},
	})
	if err != nil {
		return []models.User{}, err
	}

	if len(out.Items) == 0 {
		return []models.User{}, errors.New("not found users")
	}

	return r.Hydrate(out.Items), nil
}

func (r *UsersRepository) Save(m models.User) (string, error) {
	userID := uuid.New().String()

	color := "-"
	if m.Color != "" {
		color = m.Color
	}

	item := &dynamodb.PutItemInput{
		Item: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(userID),
			},
			"name": {
				S: aws.String(m.Name),
			},
			"age": {
				N: aws.String(strconv.Itoa(m.Age)),
			},
			"color": {
				S: aws.String(color),
			},
			"type_user": {
				N: aws.String(strconv.Itoa(m.TypeUser)),
			},
		},
		TableName: aws.String(r.table),
		//ConditionExpression: aws.String("attribute_not_exists(#document_number)"),
		//ExpressionAttributeNames: map[string]*string{
		//	"#document_number": aws.String("document_number"),
		//},
	}
	_, err := r.client.PutItem(item)

	return userID, err
}

func (r *UsersRepository) Delete(ID string) error {
	itemRemove := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(ID),
			},
		},
		TableName: aws.String(r.table),
	}

	_, err := r.client.DeleteItem(itemRemove)
	if err != nil {
		return err
	}

	return nil
}

func (r *UsersRepository) Update(m models.User) error {

	_, err := r.client.UpdateItem(&dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"id": &dynamodb.AttributeValue{
				S: aws.String(m.ID),
			},
		},
		TableName: aws.String(r.table),
		UpdateExpression: aws.String(
			`SET #name = :name, color = :color`,
		),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":name": {
				S: aws.String(m.Name),
			},
			":color": {
				S: aws.String(m.Color),
			},
		},
		ExpressionAttributeNames: map[string]*string{
			"#name": aws.String("name"),
		},
	})
	if err != nil {
		return err
	}

	return nil
}

// trigger repo
func NewUsersRepository(client *dynamodb.DynamoDB, table string) *UsersRepository {
	return &UsersRepository{client, table}
}
