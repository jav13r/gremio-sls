package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/MerqueoTech/gremio-sls/common/lib"
	"github.com/MerqueoTech/gremio-sls/users/create/v1/uc"
	"github.com/MerqueoTech/gremio-sls/users/models"
	"github.com/MerqueoTech/gremio-sls/users/repositories"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type Handler func(ctx context.Context, req Request) (events.APIGatewayProxyResponse, error)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
type Request events.APIGatewayProxyRequest

type CreateUserInterface interface {
	CreateUser(user models.User) (models.User, error)
}

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Adapter(useCase CreateUserInterface) Handler {
	return func(ctx context.Context, req Request) (events.APIGatewayProxyResponse, error) {

		requestBody := models.User{}
		err := json.Unmarshal([]byte(req.Body), &requestBody)
		if err != nil {
			fmt.Printf("Error aca %v", err)
			return events.APIGatewayProxyResponse{StatusCode: http.StatusBadRequest}, nil
		}

		userCreated, err := useCase.CreateUser(requestBody)
		if err != nil {
			switch err {
			case uc.ErrorAgeIsInvalid:
				return events.APIGatewayProxyResponse{
					StatusCode: 280,
					Body: fmt.Sprintf(
						`{
							"code": "ERROR_AGE_IS_INVALID",
									"detail": "La edad ingresada no es valida",
									"source": {
										"age" : %v
									},
						}`,
						requestBody.Age,
					),
				}, nil
			default:
				return events.APIGatewayProxyResponse{
					StatusCode: 400,
				}, nil
			}
		}

		responseJSON, err := json.Marshal(userCreated)
		if err != nil {
			return lib.JSONError(http.StatusBadRequest, err), nil
		}
		return lib.JSONResponse(http.StatusOK, responseJSON), nil
	}
}

func main() {

	// variables
	users := os.Getenv("DYNAMO_DEMO")
	if users == "" {
		panic(fmt.Sprintf("DYNAMO_DEMO is empty"))
	}

	// session aws
	session, err := session.NewSession()
	if err != nil {
		panic(fmt.Sprintf("ErrorHasOcurredCreatingAWSSession : %v", err))
	}

	// instanciar dynamo
	dynamo := dynamodb.New(session)

	// instancia repo
	userRepo := repositories.NewUsersRepository(dynamo, users)

	lambda.Start(Adapter(uc.NewCreateUserUC(userRepo)))
}
