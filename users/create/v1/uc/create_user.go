package uc

import (
	"errors"

	"github.com/MerqueoTech/gremio-sls/users/models"
)

var (
	ErrorAgeIsInvalid = errors.New("age_is_invalid")
)

type UserRepositoryInterface interface {
	Save(m models.User) (string, error)
	Find(m string) (models.User, error)
}

type CreateUser struct {
	userRepository UserRepositoryInterface
}

func (uc *CreateUser) CreateUser(user models.User) (models.User, error) {
	if user.Age == 0 {
		return models.User{}, ErrorAgeIsInvalid
	}

	userID, err := uc.userRepository.Save(user)
	if err != nil {
		return models.User{}, err
	}

	savedUser, err := uc.userRepository.Find(userID)
	if err != nil {
		return models.User{}, err
	}

	return savedUser, nil
}

func NewCreateUserUC(userRepository UserRepositoryInterface) *CreateUser {
	return &CreateUser{
		userRepository: userRepository,
	}
}
