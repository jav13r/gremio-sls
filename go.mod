module github.com/MerqueoTech/gremio-sls

go 1.15

require (
	github.com/aws/aws-lambda-go v1.6.0
	github.com/aws/aws-sdk-go v1.37.24
	github.com/google/uuid v1.2.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/xeipuuv/gojsonschema v1.2.0
)
